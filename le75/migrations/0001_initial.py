# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

class Migration(migrations.Migration):

    def getSQL(app_to_remove):
        return '''
        delete from auth_permission where content_type_id in (select id from django_content_type where app_label = '{app_label}');
        delete from django_admin_log where content_type_id in (select id from django_content_type where app_label = '{app_label}');
        delete from reversion_version where content_type_id in (select id from django_content_type where app_label = '{app_label}');
        delete from django_content_type where app_label = '{app_label}';
        delete from django_migrations where app='{app_label}';
        '''.format(app_label=app_to_remove)

    dependencies = []

    operations = [
        migrations.RunSQL('''
        drop table if exists djangocms_column_column;
        drop table if exists djangocms_column_multicolumns;
        drop table if exists djangocms_googlemap_googlemap;
        drop table if exists djangocms_inherit_inheritpageplaceholder;
        drop table if exists djangocms_style_style;
        '''),
        migrations.RunSQL(getSQL('djangocms_style')),
        migrations.RunSQL(getSQL('djangocms_column')),
        migrations.RunSQL(getSQL('djangocms_googlemap')),
        migrations.RunSQL(getSQL('djangocms_inherit'))
    ]
