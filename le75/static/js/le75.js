;(function () {
    'use strict';

    var startOffsetRange = [30, 300],
        bufferRange = [0, 150];

    function parallax() {
        var scrollY,
            mainEl = document.querySelector('.main--no-home'),
            mainWrapper = document.querySelector('.main-wrapper');
        
        function draw () {
            if (window.scrollY != scrollY) {
                scrollY = window.scrollY;

                if (document.documentElement.clientWidth) {
                    var els = document.querySelectorAll('[data-parallax="true"]');
                    for (var i = 0; i < els.length; i++) {
                        var item = els[i],
                            distance = parseInt(window.scrollY * item.getAttribute('data-speed'));
                            
                        item.style.transform = 'translate3d(0, -' + distance +'px, 0)';
                    }
                }

                if (mainEl && mainWrapper.clientHeight > mainEl.clientHeight) {
                    if ((document.documentElement.clientHeight - (mainEl.clientHeight - window.scrollY)) > 0) {
                        mainEl.classList.add('main--stick-to-bottom');
                    } else {
                        mainEl.classList.remove('main--stick-to-bottom');
                    }
                }
            }

            requestAnimationFrame(draw);
        }

        draw();
    }
    
    function updateScrollSpeed () {
        var contentHeight   = $('.main').outerHeight(),
            windowHeight    = $(window).height(),
            windowScrollDistance  = contentHeight - windowHeight;
                    
        $(".feeds__feed").each(function () {
            var $this = $(this),
                feedHeight = $this.attr('data-height');
                
            $this.attr('data-speed', (feedHeight - windowHeight) / windowScrollDistance);
        });
    }

    function updateHeaderSpacerHeight () {
        var $header = $('.main--no-home .main__header'),
            $spacer = $('.main--no-home .main__header-spacer');

        if ($header.outerHeight() != parseInt($spacer.css('height'))) {
            $spacer.css('height', $header.outerHeight() + 'px');
        }
    }

    function getXMax () {        
        if ($('body').hasClass('body--home')) {
            return $('body').innerWidth();
        } else if ($('body').hasClass('is-collapsed')) {
            return .6 * $('body').innerWidth();
        } else {
            return .25 * $('body').innerWidth();
        }
    }

    function getBufferRight ($feed) {
        return ($feed.hasClass('feeds__feed--news') || $feed.hasClass('feeds__feed--ads')) ? 350 : 100;
    }

    function calculateXPixels (percentage, $entry) {
        return parseInt(percentage * (getXMax() - getBufferRight($entry.parent())));
    }

    function calculateXPercentage (pixels, $entry) {
        return pixels / (getXMax() - getBufferRight($entry.parent()));
    } 

    window.addEventListener('load', function() {

        $('.main__nav > ul > li').click(function () {
            var $this = $(this);

            if ($this.hasClass('child--force_reveal')) {
                $this.removeClass('child--force_reveal');
            } else {
                $('.child--force_reveal').removeClass('child--force_reveal');
                $this.addClass('child--force_reveal');
            }
        });

        $('.main__nav > ul > li').mouseout(function () {
            $(this).removeClass('child--force_reveal');
        });

        $(".feeds__filter input").change(function (e) {
            var $this = $(this),
                $feeds = $('.feeds__feed[data-feed-type="' + $this.val() + '"]');

            if ($this.prop('checked')) {
                $feeds.css("visibility", "visible");
            } else {
                $feeds.css("visibility", "hidden");
            }
        });
        
        $(".feeds__entry img").on("mousedown", function (e) {
            e.preventDefault();
        }); 
        
        $(".feeds__entry a").on("mousedown", function (e) {
            e.preventDefault();
        }); 
        
        $(".feeds__entry").on("mouseover", function () {
            var $this = $(this); 
            
            $(".feeds__feed").css("z-index", "initial");
            $(".feeds__entry").css("z-index", "initial");
            
            $this.parent().css("z-index", 1);
            $this.css("z-index", 1);
        });
        
        $(".feeds__entry").on("mousedown", function (e) {
            
            var $this = $(this),
                element_x = calculateXPixels(parseFloat($this.attr("data-x")), $this) || 0,
                element_y = parseInt($this.attr("data-y")) || 0,
                x = e.screenX,
                y = e.screenY,
                dx, dy;

            $this.addClass("dragging");
            
            var move = function (e) {
                dx = e.screenX - x,
                dy = e.screenY - y;

                $this.css("transform", "translate3d(" + (element_x + dx) + "px, " + (element_y + dy) + "px, 0)");
            };
            
            var end = function () {
                $this.removeClass("dragging");
                $this.off("mousemove", move);
                $this.off("mouseup", end);
                
                // Storing new position on element
                $this.attr("data-x", calculateXPercentage((element_x + dx), $this));
                $this.attr("data-y", (element_y + dy));
            }
            
            $this.on("mouseup", end);
            $this.on("mouseleave", end);
            $this.on("mousemove", move);
        });
                    
        $('.btn-toggle').click(function () {
            $('body').toggleClass('is-collapsed');
            $('body').removeClass('reveal-nav');
            $(".feeds__entry").each(function () {
                var $entry = $(this),
                    x = $entry.attr("data-x"),
                    y = $entry.attr("data-y");
                $entry.css("transform", "translate3d(" + calculateXPixels(x, $entry) + "px, " + parseInt(y) + "px, 0)");
            });

            updateHeaderSpacerHeight();
            window.setTimeout(updateScrollSpeed);
        });

        $('.responsive-hamburger').click(function() {
            $('body').toggleClass('reveal-nav');
        })
        
        setTimeout(function () {
            var contentHeight   = $('body').outerHeight(),
                windowHeight    = $(window).height(),
                windowScrollDistance  = contentHeight - windowHeight;
            
            $(".feeds__feed").each(function () {
                var $feed   = $(this),
                    y       = startOffsetRange[0] + Math.floor(Math.random() * (startOffsetRange[1] - startOffsetRange[0]));

                $feed.find(".feeds__entry").each(function () {
                    var $entry       = $(this),
                        x           = Math.random();
                    
                    $entry.attr('style', $entry.attr('data-style'));
                    $entry.attr('data-style', '');

                    $entry.find('img[data-src]').each(function () {
                        var $img = $(this);
                        $img.attr('src', $img.attr('data-src'));
                        $img.attr('data-src', '');
                    })

                    $entry.attr({"data-x": x, "data-y": y});
                    $entry.css("transform", "translate3d(" + calculateXPixels(x, $entry) + "px, " + parseInt(y) + "px, 0)");
                    
                    y += Math.max($entry.height(), 400) + bufferRange[0] + Math.floor(Math.random() * (bufferRange[1] - bufferRange[0]));

                });
                
                $feed.attr({
                    'data-parallax': 'true',
                    'data-height': y,
                    'data-speed': (y - windowHeight) / windowScrollDistance
                });

                $feed.removeClass('feeds__feed--hidden')
            });
            
            updateHeaderSpacerHeight();

            // Start the parallax
            parallax();
        });
        
        $(window).on('resize', updateScrollSpeed);
        $(window).on('resize', updateHeaderSpacerHeight);
    });
})();