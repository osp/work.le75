#! /usr/bin/env bash

read -s -p "Enter mysql remote password: " rmtpwd
echo
read -s -p "Enter mysql local password: " lclpwd
echo
ssh -C osp@51.255.207.18 mysqldump -u osp --password=$rmtpwd --add-drop-database --databases osp_le75 | mysql -u osp --password=$lclpwd -D osp_le75
