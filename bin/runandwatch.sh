#! /usr/bin/env bash

kids=();

#<Riviera> aleray: anyway, the reason for that not working is that you don't have job control in your script; and when job control is not enabled, bash makes asynchronously started processes ignore INT.
#trap 'kill -INT "${kids[@]}"' EXIT;

trap 'kill "${kids[@]}"' EXIT;

echo "Press <Ctrl> + C to quit"

echo "Watching asset changes"
while true;
    do make | grep -v '^make:';
    sleep 1;
done & kids+=($!);

#python2 -m SimpleHTTPServer & kids+=($!);
python manage.py runserver & kids+=($!);

wait
