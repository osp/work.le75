import os.path
from fabric.api import run, local, put, cd, sudo, env, prefix
from fabric.contrib.console import confirm


env.hosts = ['osp@51.255.207.18']
env.port = 22
env.path = '/srv/www/be.leseptantecinq'


def deploy(branch='master'):
    """
    deploys to previously setup environment
    """

    with cd('%(path)s/app' % env):
        run('git pull origin %s' % branch)

        with prefix('source %(path)s/venv/bin/activate' % env):
            run('pip install -r requirements.txt')
            run('python manage.py collectstatic --noinput')

    sudo('supervisorctl restart be.leseptantecinq')
    #  run('touch %(path)s/app/le75/wsgi.py' % env)


def migrate():
    """
    runs migrations on server
    """

    with cd('%(path)s/app' % env):
        with prefix('source %(path)s/venv/bin/activate' % env):
            run('python manage.py migrate')

    run('touch %(path)s/app/le75/wsgi.py' % env)


#  def download_db():
#      """
#      synchronizes the local db and media files from the remote ones
#      """
#      local('scp -P %(port)s %(user)s@%(host)s:%(path)s/db/medor.db .' % env)


#  def download_media():
#      local("rsync -e 'ssh -p %(port)s' -avz --progress --stats %(user)s@%(host)s:%(path)s/docs/media ." % env)
