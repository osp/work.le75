from django.apps import AppConfig


class CmspluginCarouselConfig(AppConfig):
    name = 'cmsplugin_carousel'
