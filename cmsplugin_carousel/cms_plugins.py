from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _


class CarouselPlugin(CMSPluginBase):
    model = CMSPlugin
    render_template = "cmsplugin_carousel/partials/wrapper.html"
    cache = False
    allow_children = True
    #  child_classes = ['ChildCMSPlugin']

plugin_pool.register_plugin(CarouselPlugin)
