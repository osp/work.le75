import uuid
from django.db import models
from course.models import Orientation
from django.utils.translation import ugettext_lazy as _


class Feed(models.Model):
    """Registers a satellite website"""
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    url = models.URLField()
    post_selector = models.CharField(max_length=255)
    title_selector = models.CharField(max_length=255, blank=True)
    title_attribute = models.CharField(max_length=255, blank=True)
    permalink_selector = models.CharField(max_length=255, blank=True)
    permalink_attribute = models.CharField(max_length=255, blank=True)
    main_image_selector = models.CharField(max_length=255, blank=True)
    main_image_attribute = models.CharField(max_length=255, blank=True)
    excerpt_selector = models.CharField(max_length=255, blank=True)
    excerpt_attribute = models.CharField(max_length=255, blank=True)
    orientation = models.ForeignKey(Orientation, blank=True, null=True)

    def __str__(self):
        return self.url


class FeedEntry(models.Model):
    """Records a feed entry"""
    is_published = models.BooleanField(verbose_name=_("is published?"))
    feed = models.ForeignKey(Feed, blank=True, null=True)
    permalink = models.URLField()
    title = models.CharField(max_length=255)
    main_image_url = models.URLField(blank=True, null=True)
    main_image_original = models.ImageField(blank=True, null=True)
    # pub_datetime = models.DateTimeField(auto_now_add=True, auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.permalink
