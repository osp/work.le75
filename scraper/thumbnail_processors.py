from dither import dither
from PIL import ImageFilter


def traces(image):
    # img_tmp = NamedTemporaryFile(delete=True)
    # im.save(img_tmp)
    # img_tmp.flush()

    image = image.filter(ImageFilter.FIND_EDGES)
    mask=image.convert("L")
    th=150 # the value has to be adjusted for an image of interest
    mask = mask.point(lambda i: i < th and 255)
    return mask


def effect_processor(image, effect=False, **kwargs):
    """
    Appliesan effect on the source image.
    """
    if effect == "dither1":
        image = dither(image, colormap=image)

    elif effect == "dither2":
        image = dither(image, color=(255, 0, 123))

    elif effect == "dither3":
        image = dither(image, color=(65, 105, 225))

    elif effect == "traces":
        # Alternative with a dark background
        # image = dither(traces(image), color=(255, 255, 255))
        # seagreen (46, 139, 87)
        # royalblue (65,105,225)
        #image = dither(traces(image), color=(130, 130, 130))
        image = dither(image, color=(130, 130, 130))

    else:
        pass

    return image
