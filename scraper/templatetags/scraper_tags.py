from django import template
from scraper.models import FeedEntry
from course.models import Orientation
from news.models import NewsItem
from itertools import groupby
from django.utils.text import slugify

register = template.Library()

"""
    - list orientation slugs
    - if one of the orientation slugs is in the path only display that orientation
    - otherwise show everything
    
"""
def visible_feeds (orientation_list, path):
    
    show = { name: False for name in orientation_list }
    
   
    for name in orientation_list:
        if slugify(name) in path:
            show[name] = True
            return show
    
    return { name: True for name in orientation_list }
            
@register.inclusion_tag('scraper/partials/feed.html', takes_context=True)
def feed(context):
    
    """
    [
        {
            'news': {
                'objects': []
                'visible': Boolean
            }
        }
    ]
    """     
    
    entries = FeedEntry.objects.order_by('feed', '-pk')
    orientations = Orientation.objects.all()
    visible = visible_feeds([ orientation.name for orientation in orientations ] + [ 'annonces', 'nouvelles' ], context['request'].path)
    
    return {
        'feed_list': [ { 'feed': feed, 'objects': list(objects), 'visible': visible[feed.orientation.name] } for feed, objects in groupby(entries, lambda entry: entry.feed) ],
        'news': { 'visible': visible['nouvelles'], 'objects': NewsItem.objects.filter(is_feed = True).filter(is_published = True) },
        #'ads': { 'visible': visible['annonces'], 'objects': Ad.objects.filter(is_published = True) },
        'orientations': [ { 'orientation': orientation, 'visible': visible[orientation.name] } for orientation in orientations ]
    }
