# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0005_auto_20160510_1630'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feedentry',
            name='source',
        ),
    ]
