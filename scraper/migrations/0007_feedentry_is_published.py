# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0006_remove_feedentry_source'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedentry',
            name='is_published',
            field=models.BooleanField(verbose_name='is published?', default=True),
            preserve_default=False,
        ),
    ]
