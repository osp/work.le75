# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0002_auto_20160510_1619'),
        ('scraper', '0003_feed_orientation'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedentry',
            name='orientation',
            field=models.ForeignKey(blank=True, null=True, to='course.Orientation'),
        ),
    ]
