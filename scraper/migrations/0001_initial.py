# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Feed',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
                ('post_selector', models.CharField(max_length=200)),
                ('title_selector', models.CharField(max_length=200)),
                ('permalink_selector', models.CharField(max_length=200)),
                ('main_image_selector', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='FeedEntry',
            fields=[
                ('id', models.UUIDField(primary_key=True, default=uuid.uuid4, serialize=False, editable=False)),
                ('source', models.URLField()),
                ('permalink', models.URLField()),
                ('title', models.CharField(max_length=200)),
                ('main_image', models.URLField()),
            ],
        ),
    ]
