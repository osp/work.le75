# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0004_feedentry_orientation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feedentry',
            name='orientation',
        ),
        migrations.AddField(
            model_name='feedentry',
            name='feed',
            field=models.ForeignKey(null=True, to='scraper.Feed', blank=True),
        ),
    ]
