# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0002_auto_20160510_1619'),
        ('scraper', '0002_auto_20160412_1555'),
    ]

    operations = [
        migrations.AddField(
            model_name='feed',
            name='orientation',
            field=models.ForeignKey(null=True, to='course.Orientation', blank=True),
        ),
    ]
