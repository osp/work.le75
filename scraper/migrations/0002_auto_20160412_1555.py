# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='feedentry',
            old_name='main_image',
            new_name='main_image_url',
        ),
        migrations.AddField(
            model_name='feedentry',
            name='main_image_original',
            field=models.ImageField(blank=True, upload_to='', null=True),
        ),
        migrations.AddField(
            model_name='feedentry',
            name='main_image_treated',
            field=models.ImageField(blank=True, upload_to='', null=True),
        ),
    ]
