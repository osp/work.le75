from django.contrib import admin
from scraper.models import Feed, FeedEntry


class FeedAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'description',
        'url',
        'orientation',
        'post_selector',
        ('title_selector', 'title_attribute'),
        ('permalink_selector', 'permalink_attribute'),
        ('main_image_selector', 'main_image_attribute'),
        ('excerpt_selector', 'excerpt_attribute'),
    )


class FeedEntryAdmin(admin.ModelAdmin):
    list_filter = ('is_published', 'feed')


admin.site.register(Feed, FeedAdmin)
admin.site.register(FeedEntry, FeedEntryAdmin)
