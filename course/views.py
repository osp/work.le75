from course.models import TeachingUnit, Teacher, Orientation
from django.views.generic.detail import DetailView


class OrientationDetailView(DetailView):
    model = Orientation


class TeachingUnitDetailView(DetailView):
    model = TeachingUnit


class TeacherDetailView(DetailView):
    model = Teacher
