# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0010_auto_20160630_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teachingunit',
            name='common_name',
            field=models.CharField(blank=True, verbose_name='course common name', max_length=50),
        ),
        migrations.AlterField(
            model_name='teachingunit',
            name='semester',
            field=models.PositiveSmallIntegerField(verbose_name='semester', choices=[(1, 'Q1'), (2, 'Q2'), (3, 'Q1 and Q2')]),
        ),
    ]
