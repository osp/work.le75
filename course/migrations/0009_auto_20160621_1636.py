# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0008_auto_20160621_1619'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('is_mandatory', models.BooleanField(verbose_name='mandatory?')),
                ('co_required', models.TextField(verbose_name='co-required courses', blank=True)),
                ('pre_required', models.TextField(verbose_name='pre-required', blank=True)),
                ('goals', models.TextField(verbose_name='goals')),
                ('outcomes', models.TextField(verbose_name='learning outcomes')),
                ('methodology', models.TextField(verbose_name='methodology')),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('evaluation_mode', models.TextField(verbose_name='evaluation mode')),
                ('evaluation_criterias', models.TextField(verbose_name='evaluation criterias')),
                ('language', models.PositiveSmallIntegerField(verbose_name='teaching and evaluation language', choices=[(1, 'French'), (2, 'English'), (3, 'Dutch')])),
                ('references', models.TextField(verbose_name='potential sources, references and materials', blank=True)),
                ('teachers', models.ManyToManyField(verbose_name='teachers', to='course.Teacher')),
            ],
            options={
                'verbose_name': 'Course',
                'verbose_name_plural': 'Courses',
            },
        ),
        migrations.AlterModelOptions(
            name='teachingunit',
            options={'verbose_name': 'TeachingUnit', 'verbose_name_plural': 'TeachingUnits'},
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='co_required',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='content',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='evaluation_criterias',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='evaluation_mode',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='goals',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='is_mandatory',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='language',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='methodology',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='outcomes',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='pre_required',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='references',
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='teachers',
        ),
        migrations.AddField(
            model_name='course',
            name='teaching_unit',
            field=models.ForeignKey(verbose_name='teaching unit', to='course.TeachingUnit'),
        ),
    ]
