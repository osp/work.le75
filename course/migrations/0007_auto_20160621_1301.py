# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0006_auto_20160614_1306'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'Course categories', 'verbose_name': 'Course category'},
        ),
        migrations.AlterModelOptions(
            name='course',
            options={'verbose_name_plural': 'Courses', 'verbose_name': 'Course'},
        ),
        migrations.AlterModelOptions(
            name='coursetype',
            options={'verbose_name_plural': 'Course types', 'verbose_name': 'Course type'},
        ),
        migrations.AlterModelOptions(
            name='department',
            options={'verbose_name_plural': 'Working Departments', 'verbose_name': 'Working Department'},
        ),
        migrations.AlterModelOptions(
            name='orientation',
            options={'verbose_name_plural': 'Orientations', 'verbose_name': 'Orientation'},
        ),
        migrations.AlterModelOptions(
            name='teacher',
            options={'verbose_name_plural': 'Staff members', 'verbose_name': 'Staff member'},
        ),
    ]
