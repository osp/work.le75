# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0011_auto_20160701_1144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='co_required',
            field=ckeditor.fields.RichTextField(verbose_name='co-required courses', blank=True),
        ),
        migrations.AlterField(
            model_name='course',
            name='content',
            field=ckeditor.fields.RichTextField(verbose_name='content', blank=True),
        ),
        migrations.AlterField(
            model_name='course',
            name='evaluation_criterias',
            field=ckeditor.fields.RichTextField(verbose_name='evaluation criterias'),
        ),
        migrations.AlterField(
            model_name='course',
            name='evaluation_mode',
            field=ckeditor.fields.RichTextField(verbose_name='evaluation mode'),
        ),
        migrations.AlterField(
            model_name='course',
            name='goals',
            field=ckeditor.fields.RichTextField(verbose_name='goals'),
        ),
        migrations.AlterField(
            model_name='course',
            name='methodology',
            field=ckeditor.fields.RichTextField(verbose_name='methodology'),
        ),
        migrations.AlterField(
            model_name='course',
            name='outcomes',
            field=ckeditor.fields.RichTextField(verbose_name='learning outcomes'),
        ),
        migrations.AlterField(
            model_name='course',
            name='pre_required',
            field=ckeditor.fields.RichTextField(verbose_name='pre-required', blank=True),
        ),
        migrations.AlterField(
            model_name='course',
            name='references',
            field=ckeditor.fields.RichTextField(verbose_name='potential sources, references and materials', blank=True),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='bio',
            field=ckeditor.fields.RichTextField(verbose_name='short biography', blank=True),
        ),
    ]
