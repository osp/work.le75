# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('course', '0002_auto_20160510_1619'),
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(verbose_name='name', max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='teacher',
            name='bio',
            field=models.TextField(blank=True, verbose_name='bio'),
        ),
        migrations.AddField(
            model_name='teacher',
            name='image',
            field=filer.fields.image.FilerImageField(related_name='teacher_image', null=True, to='filer.Image', blank=True),
        ),
        migrations.AddField(
            model_name='teacher',
            name='phonenumber',
            field=models.CharField(blank=True, verbose_name='phone number', max_length=20),
        ),
        migrations.AddField(
            model_name='teacher',
            name='responsibilities',
            field=models.TextField(blank=True, verbose_name='responsibilities'),
        ),
        migrations.AddField(
            model_name='teacher',
            name='role',
            field=models.CharField(verbose_name='role', default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='teacher',
            name='website',
            field=models.URLField(blank=True, verbose_name='website'),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='email',
            field=models.EmailField(blank=True, verbose_name='email', max_length=254),
        ),
    ]
