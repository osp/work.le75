# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0012_auto_20160703_2338'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='teacher',
            options={'verbose_name_plural': 'Staff members', 'ordering': ['lastname', 'firstname'], 'verbose_name': 'Staff member'},
        ),
    ]
