# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0007_auto_20160621_1301'),
    ]

    operations = [
        migrations.RenameModel('Course', 'TeachingUnit'),
    ]
