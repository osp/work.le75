# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2016-11-23 10:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0020_auto_20161122_2108'),
    ]

    operations = [
        migrations.AddField(
            model_name='teachingunit',
            name='is_published',
            field=models.BooleanField(default=True, verbose_name='is published?'),
            preserve_default=False,
        ),
    ]
