# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='evaluation_criterias',
            field=models.TextField(verbose_name='evaluation criterias'),
        ),
        migrations.AlterField(
            model_name='course',
            name='pre_required',
            field=models.TextField(verbose_name='pre-required', blank=True),
        ),
    ]
