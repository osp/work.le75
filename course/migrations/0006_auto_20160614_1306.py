# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0005_teacher_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='bio',
            field=models.TextField(verbose_name='short biography', blank=True),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, null=True, verbose_name='picture', to='filer.Image', related_name='teacher_image'),
        ),
    ]
