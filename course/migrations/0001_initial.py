# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='course name')),
                ('slug', models.SlugField(help_text='Unique identifier. Allows a constant targeting of this person', unique=True, verbose_name='slug')),
                ('ue', models.PositiveIntegerField()),
                ('audience', models.PositiveSmallIntegerField(verbose_name='audience', choices=[(1, 'BAC 1'), (2, 'BAC 2'), (3, 'BAC 3')])),
                ('semester', models.PositiveSmallIntegerField(verbose_name='semester', choices=[(1, 'Q1'), (2, 'Q2')])),
                ('hours', models.PositiveSmallIntegerField(verbose_name='hours')),
                ('ects', models.PositiveSmallIntegerField(verbose_name='ects')),
                ('is_mandatory', models.BooleanField(verbose_name='mandatory?')),
                ('co_required', models.TextField(verbose_name='co-required courses', blank=True)),
                ('pre_required', models.TextField(verbose_name='pre-required')),
                ('goals', models.TextField(verbose_name='goals')),
                ('outcomes', models.TextField(verbose_name='learning outcomes')),
                ('methodology', models.TextField(verbose_name='methodology')),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('evaluation_mode', models.TextField(verbose_name='evaluation mode')),
                ('evaluation_criterias', models.TextField(verbose_name=b'evaluation criterias')),
                ('language', models.PositiveSmallIntegerField(verbose_name='teaching and evaluation language', choices=[(1, 'French'), (2, 'English'), (3, 'Dutch')])),
                ('references', models.TextField(verbose_name='potential sources, references and materials', blank=True)),
                ('category', models.ForeignKey(verbose_name='category', to='course.Category')),
            ],
        ),
        migrations.CreateModel(
            name='CourseType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='Orientation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=50, verbose_name='first name')),
                ('lastname', models.CharField(max_length=50, verbose_name='last name')),
                ('email', models.EmailField(max_length=254, verbose_name='email')),
            ],
        ),
        migrations.AddField(
            model_name='course',
            name='course_type',
            field=models.ForeignKey(verbose_name='course type', to='course.CourseType'),
        ),
        migrations.AddField(
            model_name='course',
            name='orientation',
            field=models.ManyToManyField(to='course.Orientation', verbose_name='orientation'),
        ),
        migrations.AddField(
            model_name='course',
            name='teachers',
            field=models.ManyToManyField(to='course.Teacher', verbose_name='teachers'),
        ),
    ]
