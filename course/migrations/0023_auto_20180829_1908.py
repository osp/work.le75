# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-08-29 17:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0022_auto_20161123_1143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=50, verbose_name='category name'),
        ),
    ]
