# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0009_auto_20160621_1636'),
    ]

    operations = [
        migrations.AddField(
            model_name='teachingunit',
            name='common_name',
            field=models.CharField(default='', max_length=50, verbose_name='course common name'),
            preserve_default=False,
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='category',
        ),
        migrations.AddField(
            model_name='teachingunit',
            name='category',
            field=models.ManyToManyField(to='course.Category', verbose_name='category'),
        ),
        migrations.RemoveField(
            model_name='teachingunit',
            name='course_type',
        ),
        migrations.AddField(
            model_name='teachingunit',
            name='course_type',
            field=models.ManyToManyField(to='course.CourseType', verbose_name='course type'),
        ),
        migrations.AlterField(
            model_name='teachingunit',
            name='name',
            field=models.CharField(max_length=50, verbose_name='course official name'),
        ),
    ]
