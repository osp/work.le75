from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class TeachersApp(CMSApp):
    name = _("Teacher App foo")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["course.urls.teachers"]


class TeachingUnitsApp(CMSApp):
    name = _("Teaching Units App foo")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["course.urls.teaching_units"]


apphook_pool.register(TeachersApp)
apphook_pool.register(TeachingUnitsApp)
