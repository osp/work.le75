from django.conf.urls import url
from course.views import TeachingUnitDetailView


urlpatterns = [
    url(r'^ue/(?P<slug>[-\w]+)/$', TeachingUnitDetailView.as_view(), name='teaching-unit-detail'),
]
