from django.conf.urls import url
from course.views import OrientationDetailView


urlpatterns = [
    url(r'^(?P<slug>[-\w]+)/$', OrientationDetailView.as_view(), name='orientation-detail'),
]
