from django.conf.urls import url
from course.views import TeacherDetailView


urlpatterns = [
    url(r'^(?P<slug>[-\w]+)/$', TeacherDetailView.as_view(), name='teacher-detail'),
]
