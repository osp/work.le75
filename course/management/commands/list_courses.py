from django.core.management.base import BaseCommand
from course.models import Course


class Command(BaseCommand):
    help = 'Prints out the url of all the existing UE notices'

    def handle(self, *args, **options):
        for course in Course.objects.all():
            self.stdout.write("http://localhost:8000/fr/orientations/ue/{}/".format(course.teaching_unit.slug))

