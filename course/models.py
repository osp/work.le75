from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from filer.fields.image import FilerImageField
from ckeditor.fields import RichTextField
from cms.models.pluginmodel import CMSPlugin


AUDIENCE_CHOICES = (
    (1, _("BAC 1")),
    (2, _("BAC 2")),
    (3, _("BAC 3")),
)
SEMESTER_CHOICES = (
    (1, _("Q1")),
    (2, _("Q2")),
    (3, _("Q1 and Q2")),
)

LANGUAGE_CHOICES = (
    (1, _("French")),
    (2, _("English")),
    (3, _("Dutch")),
)

YEAR_CHOICES = (
    (2015, "2015-2016"),
    (2016, "2016-2017"),
    (2017, "2017-2018"),
    (2018, "2018-2019"),
    (2019, "2019-2020"),
    (2020, "2020-2021"),
    (2021, "2021-2022"),
)

class TeachingUnit(models.Model):
    is_published =  models.BooleanField(verbose_name=_("is published?"))
    name = models.CharField(max_length=250, verbose_name=_("course official name"))
    common_name = models.CharField(max_length=50, verbose_name=_("course common name"), blank=True)
    slug = models.SlugField(unique=True, verbose_name=_("slug"), help_text=_("Unique identifier. Allows a constant targeting of this person"))
    ue = models.PositiveIntegerField()
    year = models.IntegerField(default=2015, choices=YEAR_CHOICES, verbose_name=_("academic year"))
    audience = models.PositiveSmallIntegerField(choices=AUDIENCE_CHOICES, verbose_name=_("audience"))
    orientation = models.ManyToManyField("Orientation", verbose_name=_("orientation"))
    course_type = models.ManyToManyField("CourseType", verbose_name=_("course type"))
    category = models.ManyToManyField("Category", verbose_name=_("category"))
    semester = models.PositiveSmallIntegerField(choices=SEMESTER_CHOICES, verbose_name=_("semester"))
    hours = models.PositiveSmallIntegerField(verbose_name=_("hours"))
    ects = models.PositiveSmallIntegerField(verbose_name=_("ects"))

    def __str__(self):
        return self.name

    def teacher_list(self):
        teachers = []
        for course in self.course_set.all():
            for teacher in course.teachers.all():
                if teacher not in teachers:
                    teachers.append(teacher)
        return teachers

    class Meta:
        verbose_name = _("TeachingUnit")
        verbose_name_plural = _("TeachingUnits")
        ordering = ["-year", "ue"]

    def get_absolute_url(self):
        return reverse('teaching-unit-detail', kwargs={'slug': self.slug})


class Course(models.Model):
    teaching_unit = models.ForeignKey(TeachingUnit, verbose_name=_("teaching unit"))
    teachers = models.ManyToManyField("Teacher", verbose_name=_("teachers"))
    is_mandatory = models.BooleanField(verbose_name=_("mandatory?"))
    co_required = RichTextField(verbose_name=_("co-required courses"), blank=True)
    pre_required = RichTextField(verbose_name=_("pre-required"), blank=True)
    goals = RichTextField(verbose_name=_("goals"))
    outcomes = RichTextField(verbose_name=_("learning outcomes"))
    methodology = RichTextField(verbose_name=_("methodology"))
    content = RichTextField(verbose_name=_("content"), blank=True)
    evaluation_mode = RichTextField(verbose_name=_("evaluation mode"))
    evaluation_criterias = RichTextField(verbose_name=("evaluation criterias"))
    language = models.PositiveSmallIntegerField(choices=LANGUAGE_CHOICES, verbose_name=_("teaching and evaluation language"))
    references = RichTextField(verbose_name=_("potential sources, references and materials"), blank=True)

    def __str__(self):
        return self.teaching_unit.name

    class Meta:
        verbose_name = _("Course")
        verbose_name_plural = _("Courses")

    def get_absolute_url(self):
        return reverse('teaching-unit-detail', kwargs={'slug': self.teaching_unit.slug})


class Orientation(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("name"))
    slug = models.CharField(max_length=50, verbose_name=_("slug"))

    class Meta:
        verbose_name = _("Orientation")
        verbose_name_plural = _("Orientations")
        ordering = ["name"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('orientation-detail', kwargs={'slug': self.slug})

    def get_teachers(self):
        teachers = []
        for ue in self.teachingunit_set.all():
            for course in ue.course_set.all():
                for teacher in course.teachers.all():
                    if teacher not in teachers:
                        teachers.append(teacher)
        return teachers


class CourseType(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("name"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Course type")
        verbose_name_plural = _("Course types")
        ordering = ["name"]


class Category(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("category name"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Course category")
        verbose_name_plural = _("Course categories")
        ordering = ["name"]


class Teacher(models.Model):
    firstname = models.CharField(max_length=50, verbose_name=_("first name"))
    lastname = models.CharField(max_length=50, verbose_name=_("last name"))
    slug = models.CharField(max_length=100, verbose_name=_("slug"), null=True)
    department = models.ForeignKey("Department", related_name=("teacher_department"), null=True)
    role = models.CharField(max_length=100, verbose_name=_("role"))
    responsibilities = models.TextField(verbose_name=_("responsibilities"), blank=True)
    email = models.EmailField(verbose_name=_("email"), blank=True)
    phonenumber = models.CharField(max_length=20, verbose_name=_("phone number"), blank=True)
    website = models.URLField(verbose_name=_("website"), blank=True)
    bio = RichTextField(verbose_name=_("short biography"), blank=True)
    image = FilerImageField(null=True, blank=True, related_name="teacher_image", verbose_name=_("picture"))

    def __str__(self):
        return u"%s %s" % (self.lastname, self.firstname)

    class Meta:
        verbose_name = _("Staff member")
        verbose_name_plural = _("Staff members")
        ordering = ["lastname", "firstname"]

    def get_absolute_url(self):
        return reverse('teacher-detail', kwargs={'slug': u"%s" % self.slug})


class Department(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("name"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Working Department")
        verbose_name_plural = _("Working Departments")
        ordering = ["name"]


class OrientationPluginModel(CMSPlugin):
    orientation = models.ForeignKey(Orientation, verbose_name=_("orientation"))
