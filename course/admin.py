from django.contrib import admin
from course.models import TeachingUnit, Course, Orientation, CourseType, Category, Teacher, Department
from django.utils.translation import ugettext_lazy as _
from cms.admin.placeholderadmin import FrontendEditableAdminMixin


class CourseInline(admin.StackedInline):
    model = Course
    min_num = 1
    extra = 0


class TeachingUnitAdmin(FrontendEditableAdminMixin, admin.ModelAdmin):
    actions = ['duplicate']
    inlines = [CourseInline]
    prepopulated_fields = {'slug': ('ue', 'name',)}
    list_display = (
        'ue',
        'year',
        'name',
        'audience',
        'semester',
    )
    list_display_links = ('name',)
    list_filter = (
        'year',
        'audience',
        'orientation',
        'semester',
        'category__name',
        'course_type'
    )
    filter_horizontal = ('orientation',)

    def duplicate(self, request, queryset):
        for obj in queryset:
            obj.id = None
            obj.slug += '-copy'
            obj.save()

    duplicate.short_description = "Duplicate"


class TeacherAdmin(FrontendEditableAdminMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('firstname', 'lastname')}


class OrientationAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(TeachingUnit, TeachingUnitAdmin)
admin.site.register(Orientation, OrientationAdmin)
admin.site.register(CourseType)
admin.site.register(Category)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Department)
