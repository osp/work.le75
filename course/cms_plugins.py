from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from course.models import OrientationPluginModel, Orientation
from django.utils.translation import ugettext_lazy as _


class OrientationPlugin(CMSPluginBase):
    model = OrientationPluginModel
    render_template = "course/partials/orientation_plugin.html"
    cache = True


class TeacherListPlugin(CMSPluginBase):
    model = CMSPlugin
    render_template = "course/partials/teacher-list_plugin.html"
    cache = True

    def render(self, context, instance, placeholder):
        context = super(TeacherListPlugin, self).render(context, instance, placeholder)

        queryset = Orientation.objects.all()
        orientations = {}

        for o in queryset:
            orientations[o.name] = []
            for ue in o.teachingunit_set.all():
                for course in ue.course_set.all():
                    for teacher in course.teachers.all():
                        if teacher not in orientations[o.name]:
                            orientations[o.name].append(teacher)

        context["orientations"] = orientations
        return context


plugin_pool.register_plugin(OrientationPlugin)
plugin_pool.register_plugin(TeacherListPlugin)
