SHELL := /usr/bin/env bash

all: le75/static/css/main.out.css

le75/static/css/main.out.css: le75/static/css/main.css
	./node_modules/.bin/postcss \
	--map \
	--use postcss-cssnext \
	--output $@ $<

# In the makefile:
.PHONY: watch
watch:
	while true; do make | grep -v "^make"; sleep 1; done  
