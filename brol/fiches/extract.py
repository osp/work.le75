#! /usr/bin/env python


# Copyright (C) 2016 Open Source Publishing (Alexandre Leray)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def main(src):
    import html5lib
    from cssselect import GenericTranslator, SelectorError
    from lxml import etree

    tree = html5lib.parse(src, treebuilder="lxml", namespaceHTMLElements=False)

    mydict = {}

    # <h1 id="ue-10050">UE 10050</h1>
    expression = GenericTranslator().css_to_xpath('[id^="bac"]')
    import ipdb; ipdb.set_trace()
    mydict['ue'] = tree.xpath(expression)[0].xpath("string()")

    print(mydict)

    # <h1 id="bac-1">BAC 1</h1>
    # <h1 id="graphisme">GRAPHISME</h1>
    # <h1 id="images-plurielles-imprimees">IMAGES PLURIELLES IMPRIMEES</h1>
    # <h1 id="peinture">PEINTURE</h1>
    # <h1 id="photographie">PHOTOGRAPHIE</h1>
    # <h1 id="section"> </h1>
    # <h1 id="cours-artistique-ca">Cours Artistique (CA) </h1>
    # <h1 id="cours-général-cg"><img src="media/image1.png" width="417" height="38" />Cours Général (CG)</h1>
    # <h1 id="cours-technique-ct">Cours Technique (CT)</h1>
    # <p><strong>Portfolio</strong></p>
    # <p><strong>Soutien à l’option -Méthodologie (de la recherche) -Techniques et technologies (Infographie)</strong></p>
    # <h1 id="pratiques-de-la-recherche"><em>Pratiques de la Recherche</em></h1>
    # <h1 id="section-1"></h1>
    # <h1 id="q1-et-q2">Q1 et Q2</h1>
    # <h1 id="heures-45">Heures: 45</h1>
    # <h1 id="ects-3">ECTS: 3</h1>
    # <h1 id="section-2"></h1>
    # <h1 id="professeurs">Professeurs</h1>
    # <p><strong>Danielle Seynaeve</strong></p>
    # <p><strong>danielle.seynaeve@le75.be</strong></p>
    # <p><strong>Anne De Gelas</strong></p>
    # <p><strong>Anne.degelas@le75</strong></p>
    # <p><strong>Mathieu Lecouturier</strong></p>
    # <p><strong>mathieu.lecouturier@le75.be</strong></p>
    # <h1 id="section-3"></h1>
    # <h1 id="cours-pratiques-de-la-recherche-obligatoire-intégrant-trois-approches">Cours <em>« Pratiques de la recherche »</em> obligatoire intégrant trois approches : </h1>
    # <p>-réflexion de recherche</p>
    # <p>-pratique</p>
    # <p>-le support numérique</p>
    # <h1 id="acquis-dapprentissage">Acquis d'apprentissage</h1>
    # <ul>
    # <li><p>Réflexions sur l’accumulation de références, d’informations, d’écrits, d’objets</p></li>
    # <li><p>Prendre conscience des objets accumulés (physique ou absent)</p></li>
    # <li><p>Etre capable de faire des associations, d’alimenter et explorer son imaginaire et de le communiquer à l’aide d’un support visuel : « Quel genre de ‘savoir’ peut-on tirer du ‘voir’ »</p></li>
    # <li><p>Mettre en marche et prolonger ce processus</p></li>
    # <li><p>Comprendre le rapport entre les images accumulées</p></li>
    # </ul>
    # <h1 id="méthode">Méthode</h1>
    # <ul>
    # <li><p>Présentation collégiale sous forme de 4 grandes réunions destinées à tous</p></li>
    # <li><p>Visite du Mundaneum en vue de déclencher le processus</p></li>
    # <li><p>Pédagogie de l’image en mouvement appliqué au processus choisi par l’étudiant</p></li>
    # <li><p>Rencontres régulières en groupe avec les professeurs de l’unité</p></li>
    # </ul>
    # <h1 id="mode-dévaluation">Mode d'évaluation</h1>
    # <ul>
    # <li><p>Première exercice réflexion sur l’intention de la pratique en Janvier suite à la visite du Mundaneum</p></li>
    # <li><p>Présentation d’un objet mobile construit par l’étudiant en fin du deuxième quadrimestre qui sera évalué par les trois professeurs</p></li>
    # </ul>
    # <h1 id="section-4"></h1>
    # <h1 id="critères-dévaluation">Critères d'évaluation</h1>
    # <ul>
    # <li><p>L’étudiant a intégré le processus d’approche intégrée (il ne s’agit pas d’évaluer une démarche artistique qui elle, fait partie de l’Atelier de l’option).</p></li>
    # </ul>
    # <h1 id="langue-denseignement-et-dévaluation" class="ListParagraph">Langue d'enseignement et d'évaluation </h1>
    # <ul>
    # <li><p>Français</p></li>
    # </ul>



    # walker = html5lib.getTreeWalker("dom")

    # stream = walker(dom)

    # s = html5lib.serializer.HTMLSerializer()
    # output = s.serialize(stream)

    return "\n"

    # print(repr(s.render(stream)))
    # doc = minidom.parseString(src)  # parseString also exists
    # print(doc)
    # svg = doc.firstChild

    # for path in doc.getElementsByTagName('path'):
        # d = path.getAttribute('d')
        # path.setAttribute("fill", "none")
        # path.setAttribute("stroke", "black")
        # path.setAttribute("stroke-width", "3")

        # for i in parse_path(d):
            # if isinstance(i, CubicBezier):
                # node = doc.createElement("line")
                # node.setAttribute("x1", "%s" % i.control1.real)
                # node.setAttribute("y1", "%s" % i.control1.imag)
                # node.setAttribute("x2", "%s" % i.start.real)
                # node.setAttribute("y2", "%s" % i.start.imag)
                # node.setAttribute("stroke", "blue")
                # node.setAttribute("stroke-width", "2")
                # path.parentNode.appendChild(node)

                # node = doc.createElement("line")
                # node.setAttribute("x1", "%s" % i.control2.real)
                # node.setAttribute("y1", "%s" % i.control2.imag)
                # node.setAttribute("x2", "%s" % i.end.real)
                # node.setAttribute("y2", "%s" % i.end.imag)
                # node.setAttribute("stroke", "blue")
                # node.setAttribute("stroke-width", "2")
                # path.parentNode.appendChild(node)

                # node = doc.createElement("circle")
                # node.setAttribute("cx", "%s" % i.control1.real)
                # node.setAttribute("cy", "%s" % i.control1.imag)
                # node.setAttribute("r", "4")
                # node.setAttribute("fill", "white")
                # node.setAttribute("stroke", "blue")
                # node.setAttribute("stroke-width", "2")
                # path.parentNode.appendChild(node)

                # node = doc.createElement("circle")
                # node.setAttribute("cx", "%s" % i.control2.real)
                # node.setAttribute("cy", "%s" % i.control2.imag)
                # node.setAttribute("r", "4")
                # node.setAttribute("fill", "white")
                # node.setAttribute("stroke", "blue")
                # node.setAttribute("stroke-width", "2")
                # path.parentNode.appendChild(node)

            # node = doc.createElement("circle")
            # node.setAttribute("cx", "%s" % i.start.real)
            # node.setAttribute("cy", "%s" % i.start.imag)
            # node.setAttribute("r", "8")
            # node.setAttribute("fill", "white")
            # node.setAttribute("stroke", "red")
            # node.setAttribute("stroke-width", "2")
            # path.parentNode.appendChild(node)

            # node = doc.createElement("circle")
            # node.setAttribute("cx", "%s" % i.end.real)
            # node.setAttribute("cy", "%s" % i.end.imag)
            # node.setAttribute("r", "8")
            # node.setAttribute("fill", "white")
            # node.setAttribute("stroke", "red")
            # node.setAttribute("stroke-width", "2")
            # path.parentNode.appendChild(node)


    # ret = doc.toprettyxml()
    # doc.unlink()
    # return ret


if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser(description='Parse le 75')
    parser.add_argument('infile',  nargs='?', type=argparse.FileType('r'), default=sys.stdin,
                        help="the source `.txt` file")
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout,
                                help="the file to save the result to (stdout is used if not given)")

    args = parser.parse_args()

    content = args.infile.read()
    out = main(content)
    args.outfile.write(out)
