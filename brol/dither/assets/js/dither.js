(function (window, document) {
    'use strict';
    
    window.makeCanvas = function (width, height) {
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        return canvas;
    },
 
    /*
    * Load from image element
    */
    window.fromImg = function (img) {
        var canvas  = makeCanvas(img.width, img.height),
            ctx     = canvas.getContext('2d');
                                        
        ctx.drawImage(img, 0, 0, img.width, img.height);
        return ctx.getImageData(0, 0, img.width, img.height);
    };
 
    /*
    * Inject the canvas image into an image tag as a dataURL
    */
    window.inject = function (img, el) {
        var canvas  = makeCanvas(img.width, img.height);
        canvas.getContext("2d").putImageData(img, 0, 0);
        el.src = canvas.toDataURL();
    };
 
    window.getDataOffset = function (image, x, y, channel) {
        return (image.width * y + x) * 4 + channel;
    };
 
    /*
    *      Atkins Dithering
    *
    *           x  1/8 1/8
    *      1/8 1/8 1/8
    *          1/8
    * 
    */
    window.AtkinsDitherRGB = function (el, threshold) {
        if (arguments.length < 2) {
            threshold = 128;
        } else {
            // ensure -1 < threshold < 255
            threshold = Math.min(255, Math.max(0, threshold));
        } 
        
        var img = fromImg(el);
        
        var neighbours = [
                [1,0],
                [2,0],
                [-1,1],
                [0,1],
                [1,1],
                [0,2],
            ],
            neighbourOffsets = [
                getDataOffset(img, 1, 0, 0),
                getDataOffset(img, 2, 0, 0),
                getDataOffset(img, -1, 1, 0),
                getDataOffset(img, 0, 1, 0),
                getDataOffset(img, 1, 1, 0),
                getDataOffset(img, 0, 2, 0)
            ],
            offset = 0;
        
        for (var y=0; y < img.height; y++) {
            for (var x=0; x < img.width; x++) {
                offset += 4;
                var oldValue    = [img.data[offset], img.data[offset+1], img.data[offset+2]],
                    newValue    = [(oldValue[0] > threshold) ? 255 : 0, (oldValue[1] > threshold) ? 255 : 0, (oldValue[2] > threshold) ? 255 : 0],
                    error       = [(oldValue[0] - newValue[0]) >> 3, (oldValue[1] - newValue[1]) >> 3, (oldValue[2] - newValue[2]) >> 3];
                                            
                img.data[offset]      = newValue[0];
                img.data[offset+1]    = newValue[1];
                img.data[offset+2]    = newValue[2];
                                            
                for (var n=0; n<6; n++) {
                    var nx = x + neighbours[n][0],
                        ny = y + neighbours[n][1];
                                            
                    if (nx > -1 && nx < img.width && ny < img.height) {
                        img.data[offset + neighbourOffsets[n]]     += error[0];
                        img.data[offset + neighbourOffsets[n] + 1] += error[1];
                        img.data[offset + neighbourOffsets[n] + 2] += error[2];
                    }
                }
            }
        }
        
        inject(img, el);
    };

    window.AtkinsDither = function (el, threshold) {
        if (arguments.length < 2) {
            threshold = 128;
        } else {
            // ensure -1 < threshold < 255
            threshold = Math.min(255, Math.max(0, threshold));
        } 
        
        var img = fromImg(el);
        
        var neighbours = [
                [1,0],
                [2,0],
                [-1,1],
                [0,1],
                [1,1],
                [0,2],
            ],
            neighbourOffsets = [
                getDataOffset(img, 1, 0, 0),
                getDataOffset(img, 2, 0, 0),
                getDataOffset(img, -1, 1, 0),
                getDataOffset(img, 0, 1, 0),
                getDataOffset(img, 1, 1, 0),
                getDataOffset(img, 0, 2, 0)
            ],
            offset = 0;
        
        for (var y=0; y < img.height; y++) {
            for (var x=0; x < img.width; x++) {
                offset += 4;
                
                var oldValue    = ( img.data[offset] + img.data[offset+1] + img.data[offset+2] ) / 3,
                    newValue    = (oldValue > threshold) ? 255 : 0,
                    error       = (oldValue - newValue) >> 3;
                    
                img.data[offset]      = newValue;
                img.data[offset+1]    = newValue;
                img.data[offset+2]    = newValue;
                
                for (var n=0; n < 6; n++) {
                    var nx = x+neighbours[n][0],
                        ny = y+neighbours[n][1];
                
                    if (nx > -1 && nx < img.width && ny < img.height) {
                        img.data[offset+neighbourOffsets[n]]      += error;
                        img.data[offset+neighbourOffsets[n]+1]    += error;
                        img.data[offset+neighbourOffsets[n]+2]    += error;
                    }
                }
            }
        }
        
        inject(img, el);
    };
})(window, document);