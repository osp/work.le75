document.addEventListener('DOMContentLoaded', function() {
  function draw() {
    requestAnimationFrame(draw);
    // Drawing code goes here
    scrollEvent();
  }
  draw();
  
  (function($) {
      var maxHeight = 0;
      $("div").each(function(){
          maxHeight = Math.max( maxHeight, $(this).height() );
      });
      
      $("div").each(function(){
          var speed = $(this).height() / maxHeight;
          $(this).attr('data-parallax', "true")
          $(this).attr('data-speed', speed - 0.1)
      });
  })($);
});

function scrollEvent() {
  if (!is_touch_device()){
    var viewportTop = window.scrollY;
    var windowHeight = document.documentElement.clientHeight;
    var viewportBottom = windowHeight + viewportTop;

    if (document.documentElement.clientWidth) {
      var parallax = document.querySelectorAll('[data-parallax="true"]');
      for (var i = 0; i < parallax.length; i++) {
        var item = parallax[i];
        var distance = viewportTop * item.getAttribute('data-speed');
        var sym = item.getAttribute('data-direction') === 'up' ? '-' : '';
        item.style.transform = 'translate3d(0, ' + sym + distance +'px,0)';
      }    
    }
  }
}

function is_touch_device() {
  return 'ontouchstart' in window || 'onmsgesturechange' in window;
}
