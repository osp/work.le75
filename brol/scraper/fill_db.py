#!/usr/bin/env python
# -*- coding: utf-8 -*-

from main.settings import db

pages = [
    {
        "url": "http://le75gr.tumblr.com/",
        "selectors": [
            "#wrapper main article"
        ]
    },

    {
        "url": "http://posterpanic.blogspot.be/",
        "selectors": [
            ".post"
        ]
    },

    {
        "url": "http://leseptantecinq.be/bloggraphisme/",
        "selectors": [
            ".post"
        ]
    }
]
        
for page in pages:
    db.pages.update({ 'url': page['url'] }, page, upsert=True)