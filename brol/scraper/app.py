#!/usr/bin/env python
# -*- coding: utf-8 -*-

from main.settings import db
from main import utils
from flask import Flask, render_template

app = Flask(__name__)

@app.route("/", methods=["GET"])   
def index():
    snippets = db.snippets.find()
    return render_template('index.html', snippets=snippets) 

@app.route("/images", methods=["GET"])   
def index_images():
    snippets = [ {'url': snippet['url'], 'images': utils.extract_image_paths(snippet['html']) } for snippet in db.snippets.find() ]
    return render_template('index_images.html', snippets=snippets) 

@app.route("/p", methods=["GET"])
@app.route("/paragraphs", methods=["GET"])
def index_paragraphs():
    snippets = [ {'url': snippet['url'], 'paragraphs': utils.extract_paragraphs(snippet['html']) } for snippet in db.snippets.find() ]
    return render_template('index_paragraphs.html', snippets=snippets) 

@app.route("/texts", methods=["GET"])
def index_texts():
    snippets = [ {'url': snippet['url'], 'text': utils.extract_text(snippet['html']) } for snippet in db.snippets.find() ]
    return render_template('index_texts.html', snippets=snippets) 

@app.route("/combined", methods=["GET"])
def index_combined():
    snippets = [
        {   
            'url': snippet['url'],
            'text': utils.extract_text(snippet['html']),
            'images': utils.extract_image_paths(snippet['html'])
        } for snippet in db.snippets.find()
    ]
    return render_template('index_combined.html', snippets=snippets) 


if __name__ == '__main__':
    app.run(host="localhost", port=7575, debug=True)