��    T      �  q   \            !     (     .     4     :     N     U     g     w     �     �     �  2   �     �     �     �     �     �     �                    *  	   -     7     :     G     U     e     |     �  =   �     �     �     �     
	     	     	     *	     >	     M	     U	     h	     }	     �	     �	     �	      �	     �	  
   �	     �	     �	     �	     �	     �	  	   
     
     
     "
  
   +
     6
     B
     G
     J
     S
     _
     f
     s
  +   {
     �
     �
     �
     �
     �
     �
     �
  "   �
          %      .     O     ]     c  j  k     �     �     �     �     �               .     D     R     a     g  7   l     �     �     �     �  	   �  	   �     �                         &     )     ?     V  %   r     �     �  <   �               2     E     T     j  	   �     �     �     �     �     �     �     �     �  &   �          /     7  	   :     D     K     Y     f     u     �     �     �     �     �     �     �     �     �     �     �  #   �     "     .     B     S     Y     f  	   x  )   �  
   �     �  *   �     �                  J   P       6           G   A         
       =   F   (      ;   )   +                      2   -      S      0   	   T   1                    <                             9   >   :   4   I                  L                       &       H   "   M   ,       #             $   7   '      !       C   O   R       .   3   ?      E   8   K   %   @      D              /      N            *   5   Q                  B    AdsApp BAC 1 BAC 2 BAC 3 Change to language: Course Course categories Course category Course type Course types Courses Date Date information (days, hours, opening date, etc.) Description Dutch English Ex situ App French General In situ App Orientation Orientations Q1 Q1 and Q2 Q2 Staff member Staff members Teacher App foo Teaching Units App foo TeachingUnit TeachingUnits Unique identifier. Allows a constant targeting of this person Working Department Working Departments academic year audience category category name co-required courses concerns le75? content course common name course official name course type ects email en end date (used for sorting only) evaluation mode first name fr goals hours is in the feeds? is published? last name learning outcomes link location mandatory? methodology name nl oneliner orientation period phone number picture potential sources, references and materials pre-required publication date responsibilities role semester short biography slug start date (used for sorting only) subtitle teachers teaching and evaluation language teaching unit title website Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-23 12:08+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Application annonces BAC 1 BAC 2 BAC 3 Changer la langue: Cours Intitulés génériques Intitulé générique Type de cours Types de cours Cours Date Informations de dates (jours, heures, vernissage, etc.) Description Néerlandais Anglais Application Ex situ Français Général Application In situ Orientation Orientations Q1 Q1 et Q2 Q2 Membre de l’équipe Membres de l’équipe Application Enseignants foo Application Unité d'enseignement foo Unité d'enseignement Unités d'enseignement Identifiant unique. Permet un accès pérenne à ce contenu. Département de travail Département de travail année académique s’adresse à intitulé générique intitulé générique co-requis concerne le75? contenu nom commun du cours nom officiel du cours type de cours crédits ects courriel en date de fin (uniq. pour le classement) mode d’évaluation prénom fr objectifs heures dans le flux? est publié? nom de famille acquis d’apprentissage lien lieu obligatoire? méthode nom nl phrase d’accroche orientation période numéro de téléphone portrait sources et références indicatives pré-requis date de publication responsabilités rôle quadrimestre courte biographie permalien date de début (uniq. pour le classement) sous-titre enseignant(s) langue d’enseignement et d’évaluation unité d'enseignement titre site web 