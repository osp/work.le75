from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from filer.fields.image import FilerImageField, FilerFileField
from ckeditor.fields import RichTextField


class NewsItem(models.Model):
    is_published =  models.BooleanField(verbose_name=_("is published?"))
    is_feed =  models.BooleanField(verbose_name=_("is in the feeds?"))
    title = models.CharField(max_length=160, verbose_name = _("title"))
    slug = models.SlugField(unique=True, verbose_name=_("slug"), help_text=_("Unique identifier. Allows a constant targeting of this person"))
    subtitle = models.CharField(max_length=160, verbose_name = _("subtitle"), blank=True, null=True)
    oneliner = models.CharField(max_length=255, verbose_name=_("oneliner"))
    is_75 = models.BooleanField(verbose_name=_("concerns le75?"))
    pub_date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_('publication date'))
    start_date = models.DateField(verbose_name=_('start date (used for sorting only)'))
    end_date = models.DateField(verbose_name=_('end date (used for sorting only)'))
    date_info = models.TextField(verbose_name=_("Date information (days, hours, opening date, etc.)"))
    content = RichTextField(verbose_name=_("content"))
    location = models.CharField(max_length=160, verbose_name = _("location"), blank=True, null=True)
    image = FilerImageField(null=True, blank=True, related_name="news_image")
    attachment = FilerFileField(null=True, blank=True, related_name="news_attachment")

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["-start_date",]

    def get_absolute_url(self):
        if self.is_75:
            return reverse('insitu-detail', kwargs={'slug': self.slug})
        else:
            return reverse('exsitu-detail', kwargs={'slug': self.slug})

    def display_period(self):
        if self.start_date == self.end_date:
            return u"%s" % self.start_date.strftime('%d/%m/%Y')
        else:
            return u"%s → %s" % (self.start_date.strftime('%d/%m/%Y'), self.end_date.strftime('%d/%m/%Y'))


class Ad(models.Model):
    is_published =  models.BooleanField(verbose_name=_("is published?"))
    title = models.CharField(max_length=160, verbose_name = _("title"))
    slug = models.SlugField(unique=True, verbose_name=_("slug"), help_text=_("Unique identifier. Allows a constant targeting of this person"))
    pub_date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_('publication date'))
    content = RichTextField(verbose_name=_("content"))
    link = models.URLField(max_length=200, verbose_name=_("link"), blank=True)
    image = FilerImageField(null=True, blank=True, related_name="ad_image")
    attachment = FilerFileField(null=True, blank=True, related_name="ad_attachment")

    class Meta:
        ordering = ["-pub_date",]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('annonces-detail', kwargs={'slug': self.slug})
