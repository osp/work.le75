from news.models import NewsItem, Ad
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView


class InSituListView(ListView):
    queryset = NewsItem.objects.filter(is_75=True).filter(is_published=True)
    paginate_by = 10

class ExSituListView(ListView):
    queryset = NewsItem.objects.filter(is_75=False).filter(is_published=True)
    paginate_by = 10

class AdListView(ListView):
    queryset = Ad.objects.filter(is_published=True)
    paginate_by = 10

class NewsDetailView(DetailView):
    model = NewsItem

class AdDetailView(DetailView):
    model = Ad
