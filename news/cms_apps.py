from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class InSituApp(CMSApp):
    name = _("In situ App")
    urls = ["news.urls.insitu"]

    def get_urls(self, page=None, language=None, **kwargs):
        return ["news.urls.insitu"]


class ExSituApp(CMSApp):
    name = _("Ex situ App")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["news.urls.exsitu"]


class AdsApp(CMSApp):
    name = _("AdsApp")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["news.urls.ads"]


apphook_pool.register(InSituApp)
apphook_pool.register(ExSituApp)
apphook_pool.register(AdsApp)
