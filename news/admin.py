from django.contrib import admin
from news.models import NewsItem, Ad
from django.utils.translation import ugettext_lazy as _
from cms.admin.placeholderadmin import FrontendEditableAdminMixin


class NewsItemAdmin(FrontendEditableAdminMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display_links = ('title',)
    list_display = (
        'is_published',
        'title',
        'is_75',
        'is_feed',
        'display_period',
    )
    list_filter = (
        'is_75',
        'is_published',
        'start_date',
        'end_date',
    )
    fieldsets = (
        (_('General'), {
            'fields': (
                ('is_published', 'is_feed'),
                ('title', 'subtitle', 'slug'),
                'oneliner',
            ),
        }),
        (_('Date'), {
            'fields': (
                ('start_date', 'end_date', 'date_info'),
            )
        }),
        (_('Description'), {
            'fields': (
                ('is_75', 'location'),
                ('content'),
                ('image', 'attachment')
            )
        }),
    )

    def display_period(self, obj):
        if obj.start_date == obj.end_date:
            return u"%s" % obj.start_date.strftime('%d/%m/%Y')
        else:
            return u"%s → %s" % (obj.start_date.strftime('%d/%m/%Y'), obj.end_date.strftime('%d/%m/%Y'))
    display_period.short_description = _("period")


class AdAdmin(FrontendEditableAdminMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display_links = ('title',)
    list_display = (
        'is_published',
        'title',
        'pub_date',
    )
    list_filter = (
        'is_published',
        'pub_date',
    )


admin.site.register(NewsItem, NewsItemAdmin)
admin.site.register(Ad, AdAdmin)
