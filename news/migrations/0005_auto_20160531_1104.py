# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('news', '0004_ad'),
    ]

    operations = [
        migrations.AddField(
            model_name='ad',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, to='filer.Image', null=True, related_name='ad_image'),
        ),
        migrations.AddField(
            model_name='newsitem',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, to='filer.Image', null=True, related_name='news_image'),
        ),
    ]
