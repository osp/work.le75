# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0005_auto_20160531_1104'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsitem',
            name='oneliner',
            field=models.CharField(default='', max_length=255, verbose_name='oneliner'),
            preserve_default=False,
        ),
    ]
