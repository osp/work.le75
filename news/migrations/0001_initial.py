# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NewsItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('is_published', models.BooleanField(verbose_name='is published?')),
                ('title', models.CharField(verbose_name='title', max_length=160)),
                ('slug', models.SlugField(help_text='Unique identifier. Allows a constant targeting of this person', verbose_name='slug', unique=True)),
                ('is_75', models.BooleanField(verbose_name='happens at le75?')),
                ('pub_date', models.DateTimeField(verbose_name='publication date', auto_now_add=True)),
                ('start_date', models.DateField(verbose_name='start date (used for sorting only)')),
                ('end_date', models.DateField(verbose_name='end date (used for sorting only)')),
                ('date_info', models.TextField(verbose_name='Date information (days, hours, opening date, etc.)')),
                ('content', models.TextField(verbose_name='content')),
            ],
        ),
    ]
