# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_auto_20160524_1410'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('is_published', models.BooleanField(verbose_name='is published?')),
                ('title', models.CharField(verbose_name='title', max_length=160)),
                ('slug', models.SlugField(help_text='Unique identifier. Allows a constant targeting of this person', unique=True, verbose_name='slug')),
                ('pub_date', models.DateTimeField(auto_now_add=True, verbose_name='publication date')),
                ('content', models.TextField(verbose_name='content')),
                ('link', models.URLField(blank=True, verbose_name='link')),
            ],
        ),
    ]
