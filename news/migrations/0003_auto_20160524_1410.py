# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_newsitem_subtitle'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsitem',
            name='location',
            field=models.CharField(verbose_name='location (if at le75, specify the room only)', max_length=160, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='newsitem',
            name='subtitle',
            field=models.CharField(verbose_name='subtitle', max_length=160, null=True, blank=True),
        ),
    ]
