# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('news', '0006_newsitem_oneliner'),
    ]

    operations = [
        migrations.AddField(
            model_name='ad',
            name='attachment',
            field=filer.fields.file.FilerFileField(blank=True, null=True, to='filer.File', related_name='ad_attachment'),
        ),
        migrations.AddField(
            model_name='newsitem',
            name='attachment',
            field=filer.fields.file.FilerFileField(blank=True, null=True, to='filer.File', related_name='news_attachment'),
        ),
    ]
