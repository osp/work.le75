# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsitem',
            name='subtitle',
            field=models.CharField(max_length=160, verbose_name='subtitle', null=True),
        ),
    ]
