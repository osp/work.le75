# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0007_auto_20160701_1635'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ad',
            options={'ordering': ['-pub_date']},
        ),
        migrations.AlterModelOptions(
            name='newsitem',
            options={'ordering': ['-start_date']},
        ),
        migrations.AlterField(
            model_name='ad',
            name='content',
            field=ckeditor.fields.RichTextField(verbose_name='content'),
        ),
        migrations.AlterField(
            model_name='newsitem',
            name='content',
            field=ckeditor.fields.RichTextField(verbose_name='content'),
        ),
    ]
