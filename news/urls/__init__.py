from django.conf.urls import include, url


urlpatterns = [
    url(r'^in-situ/', include('news.urls.insitu')),
    url(r'^ex-situ/', include('news.urls.exsitu')),
    url(r'^annonces/', include('news.urls.ads')),
]
