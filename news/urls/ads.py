from django.conf.urls import url
from news.views import AdListView, AdDetailView


urlpatterns = [
    url(r'^$', AdListView.as_view(), name='annonces-list'),
    url(r'^(?P<slug>[-\w]+)/$', AdDetailView.as_view(), name='annonces-detail'),
]
