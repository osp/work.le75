from django.conf.urls import url
from news.views import InSituListView, NewsDetailView


urlpatterns = [
    url(r'^$', InSituListView.as_view(), name='insitu-list'),
    url(r'^(?P<slug>[-\w]+)/$', NewsDetailView.as_view(), name='insitu-detail'),
]
