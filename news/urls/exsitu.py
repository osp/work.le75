from django.conf.urls import url
from news.views import ExSituListView, NewsDetailView


urlpatterns = [
    url(r'^$', ExSituListView.as_view(), name='exsitu-list'),
    url(r'^(?P<slug>[-\w]+)/$', NewsDetailView.as_view(), name='exsitu-detail'),
]
